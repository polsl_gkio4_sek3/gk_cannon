﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public float Speed = 0.05f;
    private bool up = true;
    public GameObject Hit;
    private System.Random  random = new System.Random();
    private bool right = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var x = right ? Speed : -Speed;
        var y = up ? Speed : -Speed;
        transform.Translate(x,0,y);
        if (transform.position.x > 20 || transform.position.x < -20)
        {
            right = !right;
        }
        if (transform.position.y > 25 || transform.position.y < 0)
        {
            up = !up;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            Instantiate(gameObject, new Vector3(random.Next(-20,20),random.Next(0,25),40), transform.rotation);
            Instantiate(Hit, transform);
            Destroy(gameObject);
            Points.Score++;
        }
    }
}
