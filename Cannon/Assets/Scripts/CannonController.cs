﻿using UnityEngine;

public class CannonController : MonoBehaviour
{
    private Rigidbody _cannonBall;
    public GameObject CannonBall;
    public GameObject Explosion;
    public float FirePower;
    public Transform ExPos;
    public Transform ShotPos;
    public float Speed;

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(0.0f, Speed * Time.deltaTime, 0.0f);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(0.0f, -Speed * Time.deltaTime, 0.0f);
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Rotate(Speed * Time.deltaTime, 0.0f, 0.0f);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Rotate(-Speed * Time.deltaTime, 0.0f, 0.0f);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Fire();
        }
    }

    private void Fire()
    {
        ShotPos.rotation = transform.rotation;
        ExPos.rotation = transform.rotation;
        var cannonBallCopy = Instantiate(CannonBall, ShotPos.position, ShotPos.rotation) as GameObject;
        _cannonBall = cannonBallCopy.GetComponent<Rigidbody>();
        _cannonBall.AddForce(transform.forward * FirePower);
        Instantiate(Explosion, ExPos.position, ExPos.rotation);
    }
}